export interface reglasProyecto {
    proyecto: string;
    e_shop: boolean;
    catalogo: boolean;
    general: boolean;
    categorias: boolean;
    buscador: boolean;
    basico: boolean;
    avanzado: boolean;
    carrito: boolean;
    vista: boolean;
    funcional: boolean;
    medio_de_pago: boolean;
    transferencia: boolean;
    tarjeta_de_credito: boolean;
    tarjeta_de_debito: boolean;
    paypal: boolean;
    administracion: boolean;
    pagina: boolean;
    crud_productos: boolean;
    crud_categorias: boolean;
    estilos: boolean;
    crud_usuarios: boolean;
}