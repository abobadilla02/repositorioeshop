import { Rule, SchematicContext, Tree, chain, noop } from '@angular-devkit/schematics';
//import { getWorkspace } from '@schematics/angular/utility/config';
import { /*NodePackageInstallTask,*/ RunSchematicTask } from '@angular-devkit/schematics/tasks';

import { reglasProyecto } from './schema';

export function ngAdd(_options: reglasProyecto): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    // 1. Checkear algunas cosas básicas
    //const workspace = getWorkspace(tree);
    //const packageJson = getPackageJsonDependency(tree, '@angular/core');
    //const projectName = _options.proyecto || workspace.defaultProject;

    _context.logger.warn('Dando inicio al proceso de derivación de producto, por favor espere que el proceso termine...');

    //console.log(workspace)
    //console.log(projectName)
    //console.log(_options);

    return chain([
      _options.categorias ? categoriasSchematic(_options) : noop(),
      _options.general ? generalSchematic(_options) : noop(),
      _options.catalogo ? catalogoSchematic(_options) : noop(),
      _options.e_shop ? eshopSchematic(_options) : noop(),
    ])(tree, _context);
  };
}

function eshopSchematic(_options: reglasProyecto): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    const setupId = _context.addTask(
      new RunSchematicTask('caracteristica-eshop', {..._options})
    );
    //3. Instalar dependencias
    //if (!_options.skipInstall) {
    //_context.addTask(new NodePackageInstallTask(), [setupId]);
    //}
    console.log(setupId);
    return tree;
  }
}

function catalogoSchematic(_options: reglasProyecto): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.addTask(
      new RunSchematicTask('caracteristica-catalogo', {..._options})
    );
    return tree;
  }
}

function generalSchematic(_options: reglasProyecto): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.addTask(
      new RunSchematicTask('caracteristica-general', {..._options})
    );
    return tree;
  }
}

function categoriasSchematic(_options: reglasProyecto): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.addTask(
      new RunSchematicTask('caracteristica-categorias', {..._options})
    );
    return tree;
  }
}