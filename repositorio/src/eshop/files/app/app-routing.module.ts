import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
    { path: '', pathMatch: 'full', redirectTo: 'catalogo' },<% if (catalogo) { %>
    { path: 'catalogo', loadChildren: () => import('./components/catalogo/catalogo.module')
        .then(mod => mod.CatalogoModule)},<%}%><% if (carrito) { %>
    { path: 'carrito', loadChildren: () => import('./components/carrito/carrito.module')
        .then(mod => mod.CarritoModule)},<%}%><% if (administracion) { %>
    { path: 'administracion', loadChildren: () => import('./components/administracion/administracion.module')
        .then(mod => mod.AdministracionModule)},<%}%>
    { path: '**', redirectTo: '' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}