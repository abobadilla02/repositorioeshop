export interface reglasEshop {
    proyecto: string;
    catalogo: boolean;
    general: boolean;
    categorias: boolean;
    buscador: boolean;
    carrito: boolean;
    medio_de_pago: boolean;
    transferencia: boolean;
    tarjeta_de_credito: boolean;
    tarjeta_de_debito: boolean;
    paypal: boolean;
    administracion: boolean;
}