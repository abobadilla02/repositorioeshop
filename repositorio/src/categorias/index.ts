import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  Tree,
  chain,
  Source,
  template,
  url,
  apply,
  move,
  mergeWith,
  filter
} from '@angular-devkit/schematics';
import { reglasCategorias } from './schema';

export function configurarCategorias(_options: any): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    console.log(tree);
    return chain([
      crearComponentesCategorias(_options)
    ]);
  };
}

function crearComponentesCategorias(_options: reglasCategorias): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Creando componentes, modelos y servicios de Categorias');
    let source: Source;
    let rutaDestino;
    const ruta = '/services/catalogo';

    if (_options.general) {
      source = url(`./files${ruta}`);
      rutaDestino = `src/app${ruta}`;
    } else {
      source = url('./files');
      rutaDestino = 'src/app';
    }

    const transformedSource: Source = apply(source, [
      _options.general ? filter(path => !path.startsWith('/producto')) : filter(() => true),
      template({
        filename: rutaDestino,
        ...strings,
        ..._options
      }),
      move(normalize(rutaDestino))
    ]);

    return mergeWith(transformedSource)(tree, _context);
  };
}