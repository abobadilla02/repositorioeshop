import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute, Router } from '@angular/router';

import { ProductosService } from 'src/app/services/catalogo/productos.service';
import { Producto } from 'src/app/models/catalogo/producto.model';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss']
})
export class ListaProductosComponent implements OnInit, OnDestroy {
  productos: Producto[];
  categoria: string;
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(
    private _productosService: ProductosService,
    private _route: ActivatedRoute,
    private _router: Router) {

    this._router.events.subscribe(() => {
      this.categoria = this._route.snapshot.paramMap.get('categoria');
      this._productosService.getProductos(this.categoria);
    });
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Subscribe to productos
    this._productosService.onProductosChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(productos => {
        this.productos = productos;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
