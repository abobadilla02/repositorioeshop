import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class ProductosService {
  private _url: string;

  onProductosChanged: BehaviorSubject<any>;

  /**
   * Productos service constructor
   */
  constructor(private _http: HttpClient) {
    //this._url = environment.urlApi;
    this.onProductosChanged = new BehaviorSubject({});
  }

  /**
   * Get productos
   */
  getProductos(categoria: string): Promise<any> {
    //const url = `${this._url}/producto`;
    let productos;

    if (categoria === 'tecnologia') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "culpa",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 39658,
          "precioOferta": 11109
        },
        {
          "id": "0ce291c0-a3c0-11ea-cc37-0242ad430002",
          "marca": "commodo",
          "nombre": "irure",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 16784,
          "precioOferta": 23369
        },
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac139494",
          "marca": "ut",
          "nombre": "nulla",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 8788,
          "precioOferta": 25293
        }
      ];
    }
    if (categoria === 'instrumentos') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "culpa",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 39658,
          "precioOferta": 11109
        }
      ];
    }
    if (categoria === 'moda') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac139238",
          "marca": "labore",
          "nombre": "officia",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 14937,
          "precioOferta": 31751
        },
        {
          "id": "0ce291c0-z2c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "fugiat",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 25737,
          "precioOferta": 16923
        }
      ];
    }
    if (categoria === 'vestuario') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac139238",
          "marca": "labore",
          "nombre": "officia",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 14937,
          "precioOferta": 31751
        },
        {
          "id": "0ce291c0-z2c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "fugiat",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 25737,
          "precioOferta": 16923
        }
      ];
    }
    if (categoria === 'mujer') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac139238",
          "marca": "labore",
          "nombre": "officia",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 14937,
          "precioOferta": 31751
        }
      ];
    }
    if (categoria === 'hombre') {
      productos = [
        {
          "id": "0ce291c0-z2c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "fugiat",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 25737,
          "precioOferta": 16923
        }
      ];
    }
    if (categoria === 'categoria_1') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11fe-bb37-0242ac130002",
          "marca": "commodo",
          "nombre": "incididunt",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 36462,
          "precioOferta": 2437
        },
        {
          "id": "0ce291c0-a3c0-11ea-jh37-0242gc110002",
          "marca": "ex",
          "nombre": "laborum",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 9063,
          "precioOferta": 21791
        },
        {
          "id": "cce291k0-a3c0-11ea-bb37-0242ac130002",
          "marca": "et",
          "nombre": "eu",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 28334,
          "precioOferta": 22992
        }
      ];
    }
    if (categoria === 'categoria_2') {
      productos = [
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac130002",
          "marca": "elit",
          "nombre": "culpa",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 39658,
          "precioOferta": 11109
        },
        {
          "id": "0ce291c0-a3c0-11ea-bb37-0242ac139238",
          "marca": "labore",
          "nombre": "officia",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 14937,
          "precioOferta": 31751
        },
        {
          "id": "0ce291c0-a3c0-11ea-jh37-0242gc110002",
          "marca": "ex",
          "nombre": "laborum",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 9063,
          "precioOferta": 21791
        },
        {
          "id": "0ce291c0-a3c0-1111-bb37-0242ac130005",
          "marca": "ex",
          "nombre": "ullamco",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 37495,
          "precioOferta": 16238
        },
        {
          "id": "0ce291c0-a3c0-11ea-bbcc-02as2ac130002",
          "marca": "voluptate",
          "nombre": "proident",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 36983,
          "precioOferta": 17603
        }
      ];
    }
    if (categoria === 'categoria_3') {
      productos = [
        {
          "id": "0cd291c3-a3c0-11ea-bb37-0242ac130002",
          "marca": "cillum",
          "nombre": "incididunt",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 33989,
          "precioOferta": 24975
        },
        {
          "id": "0ce291c0-a3c0-hhea-bb37-0242ac130095",
          "marca": "deserunt",
          "nombre": "pariatur",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 13880,
          "precioOferta": 22024
        },
        {
          "id": "0ce291c0-a3c0-11ea-jh37-0242gc110002",
          "marca": "ex",
          "nombre": "laborum",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 9063,
          "precioOferta": 21791
        },
        {
          "id": "0ce281c1-a3c0-11ea-bb37-0242ac130002",
          "marca": "tempor",
          "nombre": "exercitation",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 12772,
          "precioOferta": 30535
        },
        {
          "id": "cce291k0-a3c0-11ea-bb37-0242ac130002",
          "marca": "et",
          "nombre": "eu",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 28334,
          "precioOferta": 22992
        },
        {
          "id": "0ce291c0-a3c0-11ea-bbcc-02as2ac130002",
          "marca": "voluptate",
          "nombre": "proident",
          "imagen": "http://placehold.it/150x150",
          "precioReal": 36983,
          "precioOferta": 17603
        }
      ];
    }

    return new Promise((resolve, reject) => {
        this.onProductosChanged.next(productos);
        /*
        this._http.get(url).subscribe((response: any) => {
            this.onProductosChanged.next(response.results);
            resolve(response.results);
          }, reject);*/
    });
  }
}