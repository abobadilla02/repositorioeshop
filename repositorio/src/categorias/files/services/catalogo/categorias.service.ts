import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';

import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class CategoriasService {
  private _url: string;

  onCategoriasChanged: BehaviorSubject<any>;

  /**
   * Categorias service constructor
   */
  constructor(private _http: HttpClient) {
    //this._url = environment.urlApi;
    this.onCategoriasChanged = new BehaviorSubject({});
  }

  /**
   * Get Categorias (General)
   */
  getCategorias(): Promise<any> {
    //const url = `${this._url}/categorias`;
    const categorias = [
        {
            id: '0001',
            nombre: 'Tecnología',
            ruta: 'tecnologia'
        },
        {
            id: '0002',
            nombre: 'Instrumentos',
            ruta: 'instrumentos'
        },
        {
            id: '0003',
            nombre: 'Moda',
            ruta: 'moda'
        },
        {
            id: '0004',
            nombre: 'Vestuario',
            ruta: 'vestuario'
        },
        {
            id: '0005',
            nombre: 'Mujer',
            ruta: 'mujer'
        },
        {
            id: '0006',
            nombre: 'Hombre',
            ruta: 'hombre'
        },
        {
            id: '0007',
            nombre: 'Categoría 1',
            ruta: 'categoria_1'
        },
        {
            id: '0008',
            nombre: 'Categoría 2',
            ruta: 'categoria_2'
        },
        {
            id: '0009',
            nombre: 'Categoría 3',
            ruta: 'categoria_3'
        }
    ];

    return new Promise((resolve, reject) => {
        this.onCategoriasChanged.next(categorias);
        /*
        this._http.get(url).subscribe((response: any) => {
            this.onCategoriasChanged.next(response.results);
            resolve(response.results);
          }, reject);*/
    });
  }
}