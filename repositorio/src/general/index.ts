import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  Tree,
  chain,
  Source,
  template,
  url,
  apply,
  move,
  mergeWith
} from '@angular-devkit/schematics';
import { reglasGeneral } from './schema';

export function configurarGeneral(_options: reglasGeneral): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    console.log(tree);
    return chain([
      crearComponentesGeneral(_options)
    ]);
  };
}

function crearComponentesGeneral(_options: reglasGeneral): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Creando componentes, modelos y servicios de General');
    const source: Source = url('./files');
    const transformedSource: Source = apply(source, [
      template({
        filename: 'src/app',
        ...strings,
        ..._options
      }),
      move(normalize('src/app'))
    ]);

    return mergeWith(transformedSource)(tree, _context);
  };
}