import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
import { ActivatedRoute } from '@angular/router';

import { ProductoService } from 'src/app/services/catalogo/producto.service';
import { Producto } from 'src/app/models/catalogo/producto.model';

@Component({
  selector: 'app-detalle-producto',
  templateUrl: './detalle-producto.component.html',
  styleUrls: ['./detalle-producto.component.scss']
})
export class DetalleProductoComponent implements OnInit, OnDestroy {
  producto: Producto;
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(
    private _productoService: ProductoService,
    private _route: ActivatedRoute
  ) {
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    this._productoService.getProducto(this._route.snapshot.paramMap.get('id'));
    // Subscribe to producto
    this._productoService.onProductoChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(producto => {
        this.producto = producto;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}