import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';
<%if (categorias) {%>
import { ActivatedRoute, Router } from '@angular/router';
<%}%>

import { ProductosService } from 'src/app/services/catalogo/productos.service';
import { Producto } from 'src/app/models/catalogo/producto.model';

@Component({
  selector: 'app-lista-productos',
  templateUrl: './lista-productos.component.html',
  styleUrls: ['./lista-productos.component.scss']
})
export class ListaProductosComponent implements OnInit, OnDestroy {
  productos: Producto[];<%if (categorias) {%>
  categoria: string;<%}%>
  private _unsubscribeAll: Subject<any>;

  /**
   * Constructor
   */
  constructor(private _productosService: ProductosService<%if (categorias) {%>, private _route: ActivatedRoute, private _router: Router<%}%>) {
    <%if (categorias) {%>
    this._router.events.subscribe(() => {
      this.categoria = this._route.snapshot.paramMap.get('categoria');
      this._productosService.getProductos(this.categoria);
    });
    <%}%>
    <%if (!categorias && general) {%>
    this._productosService.getProductos();
    <%}%>
    this._unsubscribeAll = new Subject();
  }

  // -----------------------------------------------------------------------------------------------------
  // @ Lifecycle hooks
  // -----------------------------------------------------------------------------------------------------

  /**
   * On init
   */
  ngOnInit(): void {
    // Subscribe to productos
    this._productosService.onProductosChanged
      .pipe(takeUntil(this._unsubscribeAll))
      .subscribe(productos => {
        this.productos = productos;
      });
  }

  /**
   * On destroy
   */
  ngOnDestroy(): void {
    // Unsubscribe from all subscriptions
    this._unsubscribeAll.next();
    this._unsubscribeAll.complete();
  }
}
