export interface Producto {
    id: string;
    marca: string;
    nombre: string;
    descripcion?: string;
    detalle?: any[];
    imagen: string;<% if (carrito) {%>
    unidadesDisponibles?: number;<%}%>
    precioReal: number;
    precioOferta?: number;<% if (categorias) {%>
    categorias: string[];<%}%>
}