import { normalize, strings } from '@angular-devkit/core';
import {
  Rule,
  SchematicContext,
  Tree,
  chain,
  Source,
  template,
  url,
  apply,
  move,
  mergeWith
} from '@angular-devkit/schematics';
import { reglasCatalogo } from './schema';

export function configurarCatalogo(_options: reglasCatalogo): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    console.log(tree);
    return chain([
      crearModuloCatalogo(_options)
    ]);
  };
}

function crearModuloCatalogo(_options: reglasCatalogo): Rule {
  return (tree: Tree, _context: SchematicContext) => {
    _context.logger.info('Creando módulo y rutas de Catálogo');
    const source: Source = url('./files');
    const transformedSource: Source = apply(source, [
      template({
        filename: 'src/app',
        ...strings,
        ..._options
      }),
      move(normalize('src/app'))
    ]);

    return mergeWith(transformedSource)(tree, _context);
  };
}