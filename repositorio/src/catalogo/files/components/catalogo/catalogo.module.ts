import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FlexLayoutModule } from '@angular/flex-layout';

import { CatalogoRoutingModule } from './catalogo.routing';
<% if (general || categorias) { %>
import { MatCardModule } from '@angular/material/card';
import { MatDividerModule } from '@angular/material/divider';

import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { ProductoComponent } from './lista-productos/producto/producto.component';
import { DetalleProductoComponent } from './lista-productos/producto/detalle-producto/detalle-producto.component';<%}%>

@NgModule({
  imports: [
    CommonModule,
    FlexLayoutModule,
    CatalogoRoutingModule,<% if (general || categorias) { %>
    MatCardModule,
    MatDividerModule<%}%>
  ],
  exports: [
  ],
  declarations: [<% if (general || categorias) { %>
    ListaProductosComponent,
    ProductoComponent,
    DetalleProductoComponent<%}%>
  ],
  entryComponents: [
  ],
  providers: [
  ]
})
export class CatalogoModule { }
