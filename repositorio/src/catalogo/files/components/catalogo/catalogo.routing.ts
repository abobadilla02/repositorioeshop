import { NgModule }             from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
<% if (categorias || general) { %>
import { ListaProductosComponent } from './lista-productos/lista-productos.component';
import { DetalleProductoComponent } from './lista-productos/producto/detalle-producto/detalle-producto.component'<% } %>

const routes: Routes =
    [
        {
            path      : '',
            redirectTo: <% if (general) {%>'general',<%}%><% if (categorias && !general) {%>'inicio',<%}%>
            pathMatch : 'full'
        },
        {
            path     : <% if (general) {%>'general',<%}%><% if (categorias && !general) {%>'inicio',<%}%>
            component: ListaProductosComponent
        },<% if (categorias) {%>
        {
            path     : ':categoria',
            component: ListaProductosComponent
        },<%}%><%if (categorias || general) {%>
        {
            path     : 'producto/:id',
            component: DetalleProductoComponent
        }<%}%>
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
  })
  export class CatalogoRoutingModule { }
